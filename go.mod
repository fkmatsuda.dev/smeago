module gitlab.com/fkmatsuda.dev/smeago

go 1.16

require (
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/spf13/cobra v1.1.3 // indirect
	github.com/spf13/viper v1.7.1 // indirect
	gitlab.com/fkmatsuda.dev/go/fk_logger v1.0.0 // indirect
	gitlab.com/fkmatsuda.dev/go/fk_system v1.0.0 // indirect
)
