create schema if not exists i18n;

create table i18n.languages (
    language_key text,
    language_description text,
    constraint pk_languages primary key (language_key)
);

create table i18n.text_definitions (
    language_key text,
    text_key text,
    singular text,
    plural text,
    param_select_index smallint,
    constraint pk_text_definitions primary key (language_key, text_key),
    constraint uk_text_definitions__singular unique (language_key, singular),
    constraint uk_text_definitions__pural unique (language_key, plural),
    constraint fk_text_definitions__languages foreign key (language_key) references i18n.languages (language_key)
);
