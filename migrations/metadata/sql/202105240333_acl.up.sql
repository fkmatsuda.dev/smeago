create table acl.roles (
    role_name text not null,
    role_description text not null,
    constraint pk_roles primary key (role_name)
);

insert into acl.roles (role_name, role_description) values 
    ('root', 'smeago root administrator'),
    ('admin', 'smeago tenant administrator'),
    ('user', 'smeago identified user'),
    ('guest', 'smeago guest user'),
    ('user_admin', 'smeago user administrator');

create table acl.users (
    id uuid not null,
    user_name text not null,
    user_email text not null,
    active boolean default true,
    constraint pk_users primary key (id),
    constraint uk_users__name unique (user_name),
    constraint uk_users__email unique (user_email)
);

create table acl.users_groups (
    id uuid not null,
    tenant_id uuid not null,
    group_name text not null,
    active boolean default true,
    constraint pk_users_groups primary key (id),
    constraint fk_users_groups___tenants foreign key (tenant_id) references tenant.tenants (id),
    constraint uk_group_name unique (tenant_id, group_name)
);

create table acl.tpl_users__users_groups (
    user_id uuid not null,
    users_group_id uuid not null,
    constraint pk_users__users_groups primary key (user_id, users_group_id),
    constraint fk_users__users_groups___user foreign key (user_id) references acl.users (id),
    constraint fk_users__users_groups___users_group foreign key (users_group_id) references acl.users_groups (id)
);

create table acl.tpl_users__tenants__roles (
    user_id uuid not null,
    tenant_id uuid not null,
    role_name text not null,
    constraint pk_users__tenants__roles primary key (user_id, tenant_id, role_name),
    constraint fk_users__tenants__roles___user foreign key (user_id) references acl.users (id),
    constraint fk_users__tenants__roles___tenant foreign key (tenant_id) references tenant.tenants (id),
    constraint fk_users__tenants__roles___role foreign key (role_name) references acl.roles (role_name)
);

create table acl.tpl_users_groups__roles (
    users_group_id uuid not null,
    role_name text not null,
    constraint pk_users_groups__roles primary key (users_group_id, role_name),
    constraint fk_users_groups__roles___users_group foreign key (users_group_id) references acl.users_groups (id),
    constraint fk_users_groups__roles___role foreign key (role_name) references acl.roles (role_name)
);

create table acl.users_password_identities (
    user_id uuid not null,
    password_identity text not null,
    blocked boolean not null default false,
    expired boolean not null default true,
    constraint pk_users_password_identities primary key (user_id),
    constraint fk_users_password_identities__users foreign key (user_id) references acl.users (id)
);

create table acl.users_password_itentities__history (
    user_id uuid not null,
    expired_on timestamp with time zone default current_timestamp,
    password_identity text not null,
    constraint pk_users_password_itentities__history primary key (user_id, expired_on),
    constraint fk_users_password_itentities__history___password foreign key (user_id) references acl.users_password_identities (user_id)
);
