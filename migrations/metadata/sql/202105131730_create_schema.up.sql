create schema smeago;
create schema tenant;
create schema acl;

create table smeago.session_vars (
	session_id int4 not null default pg_backend_pid(),
	var_name text not null,
	var_value text not null,
	constraint pk_session_vars primary key (session_id, var_name)
);

create function smeago.set_var(p_name text, p_value text) returns void as $$
	begin
	
		insert into smeago.session_vars (session_id, var_name, var_value)
		values (pg_backend_pid(), p_name, p_value) 
		on conflict(session_id, var_name) do update set var_value = EXCLUDED.var_value;
	
		delete from smeago.session_vars where session_id not in (select pid from pg_stat_activity);
	
	end;
$$ language plpgsql;

create function smeago.get_var(p_name text, p_default_value text) returns text as $$
	declare
		v_value text;
	begin
		select coalesce(var_value, p_default_value) into v_value from smeago.session_vars 
		where session_id = pg_backend_pid() and var_name = p_name;
	
		return v_value;
	end;
$$ language plpgsql;

create table tenant.tenants (
    id uuid not null default gen_random_uuid(),
    tenant_name text not null,
    database_url text not null,
    constraint pk_tenants primary key (id),
    constraint uk_tenants__tenant_name unique (tenant_name),
    constraint uk_tenants__database_url unique (database_url)
);

create table tenant.private_keys (
    id uuid not null default gen_random_uuid(),
    tenant__id uuid not null,
    pem text not null,
    constraint pk_private_keys primary key (id),
    constraint fk_private_keys__tenants foreign key (tenant__id) references tenant.tenants(id),
    constraint uk_private_keys__pem unique (pem)
);

create table tenant.public_keys (
    id uuid not null default gen_random_uuid(),
    private_key__id uuid not null,
    pem text not null,
    default_key boolean not null default false,
    constraint pk_public_keys primary key(id),
    constraint fk_public_keys__private_keys foreign key (private_key__id) references tenant.private_keys (id) on delete cascade
);
create index ifk_public_keys__private_keys on tenant.public_keys (private_key__id);

create function tenant.public_keys__check_single_default_key() returns trigger as $$
	begin
		if tg_op = 'INSERT' then
			if not exists (select id from tenant.public_keys where default_key and new.private_key__id = private_key__id) then
				new.default_key := true;
			end if;
		end if;
		if new.default_key then
			perform smeago.set_var('default.' || coalesce(new.private_key__id, old.private_key__id), cast(new.id as text));
			update tenant.public_keys set default_key = (not default_key) where default_key and private_key__id = new.private_key__id and id <> new.id;
		end if;
		if old.default_key and not new.default_key and smeago.get_var('default.' || new.private_key__id, cast(new.id as text)) = cast(new.id as text) then 
			raise 'At least one key must be default' using ERRCODE = 'check_violation';
		end if;
		return new;
	end;
$$ language plpgsql;

create trigger public_keys__check_single_default_key before insert or update on tenant.public_keys 
	for each row execute function tenant.public_keys__check_single_default_key();
