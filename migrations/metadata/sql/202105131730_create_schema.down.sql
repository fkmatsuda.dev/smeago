drop trigger public_keys__check_single_default_key on tenant.public_keys;

drop function tenant.public_keys__check_single_default_key();

drop index tenant.ifk_public_keys__private_keys;
drop table tenant.public_keys;
drop table tenant.private_keys;
drop table tenant.tenants;

drop function smeago.get_var(text, text);

drop function smeago.set_var(text, text);

drop table smeago.session_vars;

drop schema acl;
drop schema tenant;
drop schema smeago;
