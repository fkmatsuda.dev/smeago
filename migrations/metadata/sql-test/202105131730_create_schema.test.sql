\unset ECHO
\set QUIET 1
-- Turn off echo and keep things quiet.
-- Format the output for nice TAP.
\pset format unaligned
\pset tuples_only true
\pset pager off
-- Revert all changes on failure.
\set ON_ERROR_ROLLBACK 1
\set ON_ERROR_STOP true
-- Load the TAP functions.
BEGIN;
CREATE SCHEMA tests;
CREATE FUNCTION tests.test_public_keys_default ()
    RETURNS SETOF text
    AS $$
DECLARE
    tenant_1_id uuid;
    tenant_2_id uuid;
    tenant_1_private_key_id uuid;
    tenant_2_private_key_id uuid;
    tenant_1_public_key_1_id uuid;
    tenant_1_public_key_2_id uuid;
    tenant_2_public_key_1_id uuid;
    tenant_2_public_key_2_id uuid;
    tenant_2_public_key_3_id uuid;
    result text;
BEGIN
    INSERT INTO tenant.tenants (tenant_name, database_url)
        VALUES ('test 1', 'postgresql://smeago:smeago@localhost/tenant_1')
    RETURNING
        id INTO tenant_1_id;
    INSERT INTO tenant.tenants (tenant_name, database_url)
        VALUES ('test 2', 'postgresql://smeago:smeago@localhost/tenant_2')
    RETURNING
        id INTO tenant_2_id;
    INSERT INTO tenant.private_keys (tenant__id, pem)
        VALUES (tenant_1_id, 'test private key 1 pem')
    RETURNING
        id INTO tenant_1_private_key_id;
    INSERT INTO tenant.private_keys (tenant__id, pem)
        VALUES (tenant_2_id, 'test private key 2 pem')
    RETURNING
        id INTO tenant_2_private_key_id;
    INSERT INTO tenant.public_keys (private_key__id, pem)
        VALUES (tenant_1_private_key_id, 'test tenant 1 public key 1')
    RETURNING
        id INTO tenant_1_public_key_1_id;

    SELECT
        ok (default_key,
            'With a single public key inserted it should be marked as default') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_1_public_key_1_id;
    RETURN NEXT result;

    INSERT INTO tenant.public_keys (private_key__id, pem)
        VALUES (tenant_1_private_key_id, 'test tenant 1 public key 2')
    RETURNING
        id INTO tenant_1_public_key_2_id;

    SELECT
        ok (default_key,
            'Inserting the second public key, the first one should still be marked as default') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_1_public_key_1_id;
    RETURN NEXT result;

    SELECT
        ok (not default_key,
            'Inserting the second public key, it should not be marked as default') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_1_public_key_2_id;
    RETURN NEXT result;

    update tenant.public_keys set default_key = true where id = tenant_1_public_key_2_id;

    SELECT
        ok (not default_key,
            'Once the second public key is marked as the default, the first should not be marked as the default') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_1_public_key_1_id;
    RETURN NEXT result;

    SELECT
        ok (default_key,
            'once marked as default, the second key should be the default key') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_1_public_key_2_id;
    RETURN NEXT result;

    select throws_ok('update tenant.public_keys set default_key = false where id = ''' || cast(tenant_1_public_key_2_id as text) || '''', '23514', 'At least one key must be default', 'There should be at least one default key') into result;
    return next result;

    INSERT INTO tenant.public_keys (private_key__id, pem)
        VALUES (tenant_2_private_key_id, 'test tenant 2 public key 1')
    RETURNING
        id INTO tenant_2_public_key_1_id;
    INSERT INTO tenant.public_keys (private_key__id, pem)
        VALUES (tenant_2_private_key_id, 'test tenant 2 public key 2')
    RETURNING
        id INTO tenant_2_public_key_2_id;
    INSERT INTO tenant.public_keys (private_key__id, pem)
        VALUES (tenant_2_private_key_id, 'test tenant 2 public key 3')
    RETURNING
        id INTO tenant_2_public_key_3_id;

    SELECT
        ok (default_key,
            'After inserting 3 keys the first one should be default') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_2_public_key_1_id;
    RETURN NEXT result;

    SELECT
        ok (not default_key,
            'After inserting 3 keys the second one should not be default') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_2_public_key_2_id;
    RETURN NEXT result;

    SELECT
        ok (not default_key,
            'After inserting 3 keys, the third one should not be default') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_2_public_key_3_id;
    RETURN NEXT result;

   SELECT
        ok (not default_key,
            'After inserting the keys of the second tenant, the status of the first key of the first tenant should not be changed') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_1_public_key_1_id;
    RETURN NEXT result;

    SELECT
        ok (default_key,
            'After inserting the keys of the second tenant, the status of the second key of the first tenant should not be changed') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_1_public_key_2_id;
    RETURN NEXT result;

    update tenant.public_keys set default_key = true where id = tenant_2_public_key_2_id;

    SELECT
        ok (not default_key,
            'After marking the second key of the second tenant as default the first key should not be the default key') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_2_public_key_1_id;
    RETURN NEXT result;

    SELECT
        ok (default_key,
            'After marking the second key of the second tenant as default the second key should be the default key') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_2_public_key_2_id;
    RETURN NEXT result;

    SELECT
        ok (not default_key,
            'After marking the second key of the second tenant as default the third key should not be the default key') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_2_public_key_3_id;
    RETURN NEXT result;

   SELECT
        ok (not default_key,
            'After changing the default key of the second tenant the status of the first key of the first tenant should not be changed') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_1_public_key_1_id;
    RETURN NEXT result;

    SELECT
        ok (default_key,
            'After changing the default key of the second tenant the status of the second key of the first tenant should not be changed') INTO result
    FROM
        tenant.public_keys
    WHERE
        id = tenant_1_public_key_2_id;
    RETURN NEXT result;

END
$$ language plpgsql;
--insert into tenant.public_keys (private_key__id, pem) values ('b113f37e-1e1b-46fa-8475-fd6670fdd9d1', 'teste 1');
--insert into tenant.public_keys (private_key__id, pem) values ('b113f37e-1e1b-46fa-8475-fd6670fdd9d1', 'teste 2');
--insert into tenant.public_keys (private_key__id, pem) values ('b113f37e-1e1b-46fa-8475-fd6670fdd9d1', 'teste 3');
select * from runtests('tests'::name);
ROLLBACK;

