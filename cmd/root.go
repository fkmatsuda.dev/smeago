/*
Copyright © 2021 fkmatsuda <fkmatsuda@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/fkmatsuda.dev/smeago/config"

	"github.com/spf13/viper"
)

var (
	cfgFile string
	homeDir string
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "smeago",
	Short: "Because we destroyed the ring!",
	Long:  `A generic multitenant API server with GRAPHQL, REST and JSON-RPC services.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&homeDir, "home", config.HomeDir(), fmt.Sprintf("home dir (default is %s)", config.HomeDir()))
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", config.ConfigPath(), fmt.Sprintf("config file (default is %s)", config.ConfigPath()))

}

func initConfig() {

	config.InitConfig(homeDir)

	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		viper.SetConfigFile(config.ConfigPath())
	}

	viper.BindPFlag("server.listen.port", configCmd.PersistentFlags().Lookup("listen-port"))
	viper.BindPFlag("database.host", configCmd.PersistentFlags().Lookup("db-host"))
	viper.BindPFlag("database.port", configCmd.PersistentFlags().Lookup("db-port"))
	viper.BindPFlag("database.user", configCmd.PersistentFlags().Lookup("db-user"))
	viper.BindPFlag("database.password", configCmd.PersistentFlags().Lookup("db-password"))
	viper.BindPFlag("database.name", configCmd.PersistentFlags().Lookup("metadata-db"))

	viper.SetEnvPrefix("SMGO")
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}

}
