#!make
.DEFAULT_GOAL := all

include .testenv

define run_sql_tests =
	
endef

DIRS = test_report bin/linux/amd64

$(shell mkdir -p $(DIRS))

bindata:
	go-bindata -o migrations/binsql.go -nometadata -nomemcopy -pkg migrations migrations/metadata/sql 

migrations-test:
	dropdb -h $(PG_HOST) -U $(PG_USER) --if-exists $(PG_DATABASE)
	createdb -h $(PG_HOST) -U $(PG_USER) $(PG_DATABASE)
	psql -h $(PG_HOST) -U $(PG_USER) -d $(PG_DATABASE) -f $(TAP_SCRIPT)
	migrate -source file://migrations/metadata/sql -database "postgresql://$(PG_USER):$(PG_PASSWORD)@$(PG_HOST)/$(PG_DATABASE)?sslmode=disable" up
	pg_prove -h $(PG_HOST) -U $(PG_USER) -d $(PG_DATABASE) -v --failures --formatter TAP::Formatter::JUnit migrations/metadata/sql-test/*.test.sql > test_report/pg-report.xml
	migrate -source file://migrations/metadata/sql -database "postgresql://$(PG_USER):$(PG_PASSWORD)@$(PG_HOST)/$(PG_DATABASE)?sslmode=disable" down -all
	dropdb -h $(PG_HOST) -U $(PG_USER) --if-exists $(PG_DATABASE)

test: bindata
	go test -v 2>&1 | go-junit-report -set-exit-code > test_report/go-report.xml

flutter-test:
	(cd smeago_app && flutter test --reporter json | tojunit > ../test_report/flutter-report.xml)

test-all: migrations-test test flutter-test

build: bindata
	go build -o bin/linux/amd64/smeago

all: test-all build

