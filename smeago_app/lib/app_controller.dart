import 'package:flutter/cupertino.dart';

class AppController extends ChangeNotifier {
  bool _darkTheme = false;

  static AppController instance = AppController();

  changeTheme() {
    _darkTheme = !_darkTheme;
    notifyListeners();
  }

  bool isDarkTheme() {
    return _darkTheme;
  }
}
