package config

import (
	"fmt"
	"log"
	"os"

	home "github.com/mitchellh/go-homedir"
	fklogger "gitlab.com/fkmatsuda.dev/go/fk_logger"
	fksystem "gitlab.com/fkmatsuda.dev/go/fk_system"
)

var (
	homeDir      string
	configDir    string
	logDir       string
	logPath      string
	errorLogPath string
	configPath   string
)

// InitConfig initializes configuration
func InitConfig(dir string) {

	var err error

	if dir == "" {
		homeDir = HomeDir()
	} else {
		homeDir = dir
		configDir = ""
		configPath = ""
	}

	const (
		logFile      = "smeago.log"
		errorLogFile = "smeago.error.log"
	)

	logDir = fmt.Sprintf("%s%s%s", homeDir, fksystem.DirSeparator(), "log")
	logPath = fmt.Sprintf("%s%s%s", logDir, fksystem.DirSeparator(), logFile)
	errorLogPath = fmt.Sprintf("%s%s%s", logDir, fksystem.DirSeparator(), errorLogFile)

	fksystem.EnsureDir(ConfigDir())
	fksystem.EnsureDir(logDir)

	file, err := os.OpenFile(logFile, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0622)
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()

	efile, err := os.OpenFile(errorLogFile, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0622)
	if err != nil {
		log.Panic(err)
	}
	defer efile.Close()

	fklogger.RegisterAppender(fklogger.NewFileAppender(logDir, logFile, errorLogFile, 1024))
	fklogger.RegisterAppender(fklogger.ConsoleAppender())
}

// Config returns current config directory
func ConfigDir() string {
	if configDir == "" {
		configDir = fmt.Sprintf("%s%s%s", HomeDir(), fksystem.DirSeparator(), "etc")
	}
	return configDir
}

// ConfigPath returns current config file path
func ConfigPath() string {
	if configPath == "" {
		configPath = fmt.Sprintf("%s%s%s", ConfigDir(), fksystem.DirSeparator(), "smeago.json")
	}
	return configPath
}

// HomeDir returns current smeago home directory
func HomeDir() string {
	var err error
	if homeDir == "" {
		homeDir, err = home.Dir()
		if err != nil {
			log.Panic(err)
		}

		homeDir = fmt.Sprintf("%s%s%s", homeDir, fksystem.DirSeparator(), ".smeago")

	}
	return homeDir
}
